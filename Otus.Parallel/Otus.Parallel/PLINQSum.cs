using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Otus.Parallel
{
    public static class PLINQSum
    {
        public static long Sum(IEnumerable<int> data)
        {
            Stopwatch stopWatch = new();
            long sum = 0;
            
            stopWatch.Start();
            sum = data.AsParallel().Sum();            
            stopWatch.Stop();

            Console.WriteLine($"PLINQ sum time - {stopWatch.Elapsed}");
            return sum;
        } 
    }
}