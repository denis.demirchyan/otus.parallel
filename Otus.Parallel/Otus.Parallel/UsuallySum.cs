using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Otus.Parallel
{
    public static class UsuallySum
    {
        public static long Sum(IEnumerable<int> data)
        {
            Stopwatch stopWatch = new();
            long sum = 0;
            
            stopWatch.Start();
            
            foreach (var i in data)
            {
                sum += i;
            }
            
            stopWatch.Stop();

            Console.WriteLine($"Usually sum time - {stopWatch.Elapsed}");
            return sum;
        } 

    }
}