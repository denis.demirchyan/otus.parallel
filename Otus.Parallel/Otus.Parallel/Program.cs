﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Otus.Parallel
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = GenerateData(100000000);

            var usuallySum = UsuallySum.Sum(data);
            Console.WriteLine($"Usually sum: {usuallySum}");

            var threadsSum = ThreadsSum.Sum(data);
            Console.WriteLine($"Threads sum: {threadsSum}");

            var plinqSum = PLINQSum.Sum(data);
            Console.WriteLine($"PLINQ sum: {plinqSum}");
        }

        private static List<int> GenerateData(int count = 100000)
        {
            List<int> data = new(count);
            Random random = new();
            
            for (var i = 0; i < count; ++i)
            {
                data.Add(random.Next(0, 10));
            }

            return data;
        }
    }
}