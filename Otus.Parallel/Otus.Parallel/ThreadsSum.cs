using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Otus.Parallel
{
    public static class ThreadsSum
    {
        public static long Sum(IEnumerable<int> data, int threadsCount = 4)
        {
            Stopwatch stopWatch = new();
            List<Thread> threads = new();
            var chunksSum = new long[threadsCount];
            var waitHandles = new WaitHandle[threadsCount];
            var index = 0;
            
            var chunks = Slice(data, threadsCount);

            stopWatch.Start();
            
            foreach (var chunk in chunks)
            {
                waitHandles[index] = new AutoResetEvent(false);
                
                var chunkIndex = index;
                var thread = new Thread(() =>
                {
                    var sum = 0;
                    foreach (var i in chunk)
                    {
                        sum += i;
                    }

                    chunksSum[chunkIndex] = sum;
                    var autoResetEvent = (AutoResetEvent) waitHandles[chunkIndex];
                    autoResetEvent.Set();
                });
                
                thread.Start();
                threads.Add(thread);
                ++index;
            }
            WaitHandle.WaitAll(waitHandles);
            var sum = chunksSum.Sum();

            stopWatch.Stop();

            Console.WriteLine($"Threads sum time - {stopWatch.Elapsed}");
            return sum;
        }
        
        private static List<List<int>> Slice(IEnumerable<int> data, int chunkCount)
        {
            var dataList = data.ToList();
            var chunkSize = dataList.Count / chunkCount;
            
            if (dataList.Count % chunkCount != 0)
            {
                ++chunkSize;
            }

            var chunks = new List<List<int>>();

            for (var i = 0; i < dataList.Count; i += chunkSize)
            {
                chunks.Add(GetChunk(dataList, i, chunkSize));
            }
            
            return chunks;
        }

        private static List<int> GetChunk(IEnumerable<int> data, int start, int count)
        {
            var dataList = data.ToList();
            if (start + count > dataList.Count)
            {
                count = dataList.Count - start;
            }
            var chunk = new int[count];
            dataList.CopyTo(start, chunk, 0, count);
            return chunk.ToList();
        }


    }
}